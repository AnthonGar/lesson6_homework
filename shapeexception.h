#pragma once
#include <exception>

class shapeException : public std::exception
{
//everybody inherets that and it must be public for all.
public:
	virtual const char* what() const
	{
		return "This is a shape exception!";
	}
};