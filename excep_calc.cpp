#include <iostream>

#define MSG "This user is not authorized to access 8200,\nplease enter different numbers, or try to get clearence in 1 year."

using namespace std;

int add(int a, int b) {
	if (a + b == 8200)
	{
		throw(string(MSG));
	}
	return a + b;
}

int  multiply(int a, int b) {
	int sum = 0;
	for (int i = 0; i < b; i++) {
		sum = add(sum, a);
	};
	return sum;
}

int  pow(int a, int b) {
	int exponent = 1;
	for (int i = 0; i < b; i++) {
		exponent = multiply(exponent, a);
	};
	return exponent;
}

int main(void) {
	try
	{
		cout << add(4100, 0) << endl;
	}
	catch (string msg)
	{
		cerr << MSG << endl;
	}

	try
	{
		cout << add(4100, 4100) << endl;
	}
	catch (string msg)
	{
		cerr << MSG << endl;
	}

	try
	{
		cout << multiply(4100, 0) << endl;
	}
	catch (string msg)
	{
		cerr << MSG << endl;
	}

	try
	{
		cout << multiply(8200, 1) << endl;
	}
	catch (string msg)
	{
		cerr << MSG << endl;
	}

	try
	{
		cout << pow(4100, 0) << endl;
	}
	catch (string msg)
	{
		cerr << MSG << endl;
	}

	try
	{
		cout << pow(8200, 1) << endl;
	}
	catch (string msg)
	{
		cerr << MSG << endl;
	}

	system("pause");
}