#pragma once

#include <exception>
#include <string>

class InputException : public std::exception
{
private:
	std::string _msg;

public:
	virtual const char* what() const
	{
		return _msg.c_str();
	}

	InputException(std::string msg)
		: _msg(msg)
	{}

	InputException()
		: InputException("This is an input exception")
	{}
};