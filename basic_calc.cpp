#include <iostream>

#define MSG "This user is not authorized to access 8200,\nplease enter different numbers, or try to get clearence in 1 year."

using namespace std;

int add(bool & toPrint, int a, int b) {
	if (a + b == 8200)
	{
		cout << MSG << endl;
		toPrint = false;
	}
	else
		toPrint = true;

	return a + b;
}

int  multiply(bool & toPrint, int a, int b) {
  int sum = 0;
  for(int i = 0; i < b; i++) {
    sum = add(toPrint,sum, a);
	if (!toPrint)
		break;
  };
  return sum;
}

int  pow(bool & toPrint, int a, int b) {
  int exponent = 1;
  for(int i = 0; i < b; i++) {
    exponent = multiply(toPrint, exponent, a);
	if (!toPrint)
		break;
  };
  return exponent;
}

//This func will print the resault if the number 8200 haven't showed up.
//In: the number to print and the flag
void print(bool & toPrint, int a)
{
	if (toPrint)
		cout << a << endl;
}

int main(void) {
	bool toPrint;

	cout << "------------Errors------------" << endl;

	print(toPrint, add(toPrint, 4100, 4100));
	print(toPrint, multiply(toPrint, 8200, 1));
	print(toPrint, pow(toPrint, 8200, 1));

	cout << "------------corrects------------" << endl;

	print(toPrint, add(toPrint, 0, 4100));
	print(toPrint, multiply(toPrint, 0, 1));
	print(toPrint, pow(toPrint, 200, 1));
}